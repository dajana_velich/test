#!/usr/bin/env groovy
import groovy.json.JsonSlurperClassic

JsonSlurperClassic getJSONBitbucketPayload() {
  def payload = env.BITBUCKET_PAYLOAD
  def json = new JsonSlurperClassic().parseText(payload)  
  return json
}

String getPRCommitId() {
    def payload = getJSONBitbucketPayload()
    def commitId = "${payload.pullrequest.source.commit.hash}"
    return commitId
}

String getPRAuthor() {
    def payload = getJSONBitbucketPayload()   
    def actor = "${payload.pullrequest.author.nickname}"
    return actor
}

String getPRTitle() {
  def payload = getJSONBitbucketPayload()   
  def actor = "${payload.pullrequest.title}"
  return actor
}


String getPRDescription() {
  def payload = getJSONBitbucketPayload()   
  def actor = "${payload.pullrequest.description}"
  return actor
}

void notifyMasterBranchForStatus(String buildStatus) {
  bitbucketStatusNotify(
    buildState: buildStatus       
  )
}

void notifySourceBranchForStatus(String buildStatus, String repoName, String commitId) {
  bitbucketStatusNotify(
    buildState: buildStatus,
    repoSlug: repoName,
    commitId: commitId       
  )
}

final SUCCESS_COLOR = "#c2e8c2"
final FAILURE_COLOR = "#f15656"

void sendEmailForPullRequestJenkinsBuild(boolean isBuildSuccessful) {

  def color = "";
  if (isBuildSuccessful) {
    color = "#c2e8c2"
  } else {
    color = "#f15656"
  }
  //
  emailext attachLog: true,
    subject: 'Pull Request #$BITBUCKET_PULL_REQUEST_ID - Jenkins Build Aurora Onguard', 
    to: 'dajana.velich@gmail.com',
    body: "<html> <body> <head> <style>" +
      "li {" +
      "  margin-bottom: 1em;" +
      "}" +
      ".container {" +
      "  background-color: #f7f5f5;" +
      "  font-size: medium; " +
      "  padding-top: 20px;" +
      "  padding-bottom: 20px;" +
      "}" +
      ".wrapper {" +
      "  background-color: ${color};" +
      "  padding: 2px;" +
      "}" +
      "</style></head>" +
      "<h2>Jenkins Build #${env.BUILD_NUMBER} caused by <a href='${env.BITBUCKET_PULL_REQUEST_LINK}'> Pull Request </a> </h2> </br>" +
      "<div class='container'> <ul>" +
      "<li>  <b>Source branch</b> ${env.BITBUCKET_SOURCE_BRANCH} <b> -> Target branch</b> ${env.BITBUCKET_TARGET_BRANCH} </li>" +
      "<li> <b>Pull Request Info</b> <br><br>" +
        "<b>Title:</b> ${PR_TITLE} <br>" +
        "<b>Description:</b> ${PR_DESCRIPTION} <br>" +
        "<b>Author:</b> ${PR_AUTHOR}" + 
      "</li>" +   
      "<li> <b>Build status:</b> <u class='wrapper'><i>" + '$BUILD_STATUS' + "</i></u>. </li>" + 
      "<li> <b>Test results:</b> ${env.BUILD_URL}"  + "testReport/ </li>" +
      "</ul> </div> </body> </html>"      
}

String sendEmailForNightlyJenkinsBuild() {

}

properties([
  pipelineTriggers([
    // some UPDATE YEAH
    [
      $class: 'BitBucketPPRTrigger',
      triggers : [
        [
          $class: 'BitBucketPPRPullRequestTriggerFilter',
          actionFilter: [
            $class: 'BitBucketPPRPullRequestCreatedActionFilter',
          ]
        ],
        [
          $class: 'BitBucketPPRPullRequestTriggerFilter',
          actionFilter: [
            $class: 'BitBucketPPRPullRequestApprovedActionFilter',
          ]
        ],
        [
          $class: 'BitBucketPPRPullRequestTriggerFilter',
          actionFilter: [
            $class: 'BitBucketPPRPullRequestUpdatedActionFilter',
          ]
        ],
        [
          $class: 'BitBucketPPRPullRequestTriggerFilter',
          actionFilter: [
            $class: 'BitBucketPPRPullRequestMergedActionFilter',
          ]
        ],
        [
          $class: 'BitBucketPPRRepositoryTriggerFilter',
          actionFilter: [
            $class: 'BitBucketPPRRepositoryPushActionFilter',
            triggerAlsoIfNothingChanged: true,
            triggerAlsoIfTagPush: false,
            allowedBranches: ""
          ]
        ]
      ]
    ]
  ])
])

final MANUAL_BUILD_CAUSE = 'hudson.model.Cause$UserIdCause'
final NIGHTLY_BUILD_CAUSE = 'hudson.triggers.TimerTrigger$TimerTriggerCause'
final PULL_REQUEST_CREATED_BUILD_CAUSE = 'io.jenkins.plugins.bitbucketpushandpullrequest.cause.pullrequest.cloud.BitBucketPPRPullRequestCreatedCause'
final PULL_REQUEST_UPDATED_BUILD_CAUSE = 'io.jenkins.plugins.bitbucketpushandpullrequest.cause.pullrequest.cloud.BitBucketPPRPullRequestUpdatedCause'
final SUCCESSFUL = 'SUCCESSFUL'
final IN_PROGRESS = 'INPROGRESS'
final FAILED = 'FAILED'

pipeline {

    environment {

      BUILD_CAUSE = ""
      REPOSITORY_NAME = "test"       
      PR_COMMIT_ID = getPRCommitId()
      PR_AUTHOR = getPRAuthor() 
      PR_TITLE = getPRTitle()
      PR_DESCRIPTION = getPRDescription()
    }

    agent any

    stages {

        stage ('Checkout') {          
          steps {
            script {
              
              def causes = currentBuild.getBuildCauses();
              causes.each {
                cause -> 
                  if (cause._class == MANUAL_BUILD_CAUSE) {
                    BUILD_CAUSE = MANUAL_BUILD_CAUSE
                  } else if (cause._class == NIGHTLY_BUILD_CAUSE) {
                    BUILD_CAUSE = NIGHTLY_BUILD_CAUSE
                  } else if (cause._class == PULL_REQUEST_UPDATED_BUILD_CAUSE) {
                    BUILD_CAUSE = PULL_REQUEST_UPDATED_BUILD_CAUSE
                  } else if (cause._class == PULL_REQUEST_CREATED_BUILD_CAUSE) {
                    BUILD_CAUSE = PULL_REQUEST_CREATED_BUILD_CAUSE
                  }
              }

              if (BUILD_CAUSE == PULL_REQUEST_CREATED_BUILD_CAUSE || BUILD_CAUSE == PULL_REQUEST_UPDATED_BUILD_CAUSE) {
                
                bitbucketStatusNotify(
                buildState: 'INPROGRESS',
                repoSlug: "${REPOSITORY_NAME}",
                commitId: "${PR_COMMIT_ID}")

                git branch: "${env.BITBUCKET_SOURCE_BRANCH}",
                credentialsId: 'dbdf370b-ca6a-4e77-9173-de36f610df2e',
                url: 'https://dajana_velich@bitbucket.org/dajana_velich/test.git'
              
              } else {

                  bitbucketStatusNotify(
                  buildState: 'INPROGRESS')
              
              }
            }
          }
         
          
        }

        stage('Build') {
            steps {



                script {
                    
                    echo 'Build'
                }
            }
        }

        stage('Test') {        
            steps {
                script {

                   echo 'Test'
                   
                }
            }
        }
    }

    post  {

        // always {
            // node('master') {
            //     echo 'Master hello'
            // }
        // }
//
        // always {
        //   script {
        //     if (BUILD_CAUSE == PULL_REQUEST_CREATED_BUILD_CAUSE || BUILD_CAUSE == PULL_REQUEST_UPDATED_BUILD_CAUSE) {  
        //         sendEmailForPullRequestJenkinsBuild()              
        //     }
        //   }
        // }


        success {
          script {
            if (BUILD_CAUSE == PULL_REQUEST_CREATED_BUILD_CAUSE || BUILD_CAUSE == PULL_REQUEST_UPDATED_BUILD_CAUSE) {    
              sendEmailForPullRequestJenkinsBuild(true)                         
              notifySourceBranchForStatus(SUCCESSFUL, "${REPOSITORY_NAME}", "${PR_COMMIT_ID}")
            } else {
              notifyMasterBranchForStatus(SUCCESSFUL)
            }
          }
        }

        unstable {
          script {
            if (BUILD_CAUSE == PULL_REQUEST_CREATED_BUILD_CAUSE || BUILD_CAUSE == PULL_REQUEST_UPDATED_BUILD_CAUSE) { 
              sendEmailForPullRequestJenkinsBuild(false)                            
              notifySourceBranchForStatus(FAILED, "${REPOSITORY_NAME}", "${PR_COMMIT_ID}")
            } else {
              notifyMasterBranchForStatus(FAILED)
            }
          }
        }

        failure {
          script {
            if (BUILD_CAUSE == PULL_REQUEST_CREATED_BUILD_CAUSE || BUILD_CAUSE == PULL_REQUEST_UPDATED_BUILD_CAUSE) {   
              sendEmailForNightlyJenkinsBuild(false)            
              notifySourceBranchForStatus(FAILED, "${REPOSITORY_NAME}", "${PR_COMMIT_ID}")
            } else {
              notifyMasterBranchForStatus(FAILED)
            }
          }
        }
        
    }
   
}

// #!/usr/bin/env groovy

// String getBuildLog() {
//     return currentBuild.rawBuild.getLogFile().getText()
// }

// void runTests(String[] testFilters) {
//     String testFilter = "";
//     for(int i = 0; i < testFilters.length ; i++) {
//         testFilter += testFilters[i];
//         if (i != testFilters.length - 1) {
//             testFilter += ":"
//         }
//     }
//     sh 'sudo ./onguard_test -t ../../testdata --num-calls 1 --gtest_filter=' + testFilter + ' --gtest_output=xml:reports/results.xml'                        
// }

// pipeline {

//     agent any

//     options {
//         timestamps()
//     }

//     stages {

//         // just for test
//         /** Checkout stage is commented out since we can use it for local Jenkinsfile change (in Jenkins UI). Otherwise
//             we are having our project set up on a pipeline that gets the Pipeline script from previosly set up SCM **/

//         //   stage('Checkout') {
//         //      steps {
//         //         git branch: 'jenkins',
//         //             credentialsId: 'GIT_ACCESS',
//         //             url: 'git@bitbucket.org:auroralabscloud/aurora-onguard.git'
//         //         sh 'ls -lat'
//         //      }
//         //   }

//         stage('Build') {
//             steps {
//                 script {
                    
//                     def exists = fileExists 'build/'
//                     if (exists) {
                        
//                         echo 'Build directory exists'
//                         dir ('build') {
//                             sh 'make'
//                         }
                        
//                     } else {
                        
//                         echo 'Build directory doesn\'t exist ... installing dependencies'
//                         dir ('install') {
//                             sh './install.sh'
//                         }
                                  
//                         dir ('testdata/uds_simulator') {
//                             //  install uds simulator for running tests
//                             sh 'npm install'
//                         }
                        
//                         dir ('build') {
//                             sh 'cmake .. -DBUILD_TEST=1 ; make clean ; make'
//                         }
//                     }
//                 }
//             }
//         }

//         stage('Test') {        
//             steps {
//                 script {

//                     def exists = fileExists 'build/test/reports'
//                     if (!exists) {
//                         dir('build/test') {
//                             sh 'mkdir reports'
//                         }
//                     }    

//                     def ARCH = sh (
//                         script: 'uname --m',
//                         returnStdout: true
//                     ).trim()
//                     echo "Arch is : ${ARCH}"

//                     dir ('build/test') {
                        
//                         // clean reports folder where artifacts from the previous build are being stored
//                         dir ('reports') {
//                             sh 'rm -rf *'
//                         }
                        
//                         runTests('CSALTestParsing.PerformanceDoorAdapter2MBSleep',
//                             'CSALTestParsing.PerformanceDoorAdapter5MBSleep',
//                             'CSALTestParsing.PerformanceDoorAdapter10MBSleep',
//                             'CSALTestDaimler.*');
//                         if (!ARCH.contains('x86')) {
//                             runTests('CSALTestAddressRange.*',
//                             'CSALTestCyclesCpp.*',
//                             'CSALTestCycles.*',
//                             'CSALTestAurora.*',
//                             'CSALTestCFICpp.*',
//                             'CSALTestFake.*',
//                             'CSALTestFuncCall.*',
//                             'CSALTestParameterSet.*',
//                             'CSALTestProdMulti.*',
//                             'CSALTestProd.*',
//                             'CSALTestDaimler.*');
//                         }
//                     }
                  
//                 }
//             }
//         }
//     }

//     post {
//             always {

//                 script {
//                     writeFile(file: 'build/test/reports/build.log', text: getBuildLog())
//                 }

//                 // archiving artifacts for future master-slave configuration only
//                 archiveArtifacts artifacts: 'build/test/reports/build.log', fingerprint: true

//                 xunit([
//                 GoogleTest(
//                     deleteOutputFiles: false,
//                     failIfNotNew: false, 
//                     pattern: 'build/test/reports/*.xml', 
//                     skipNoTestFiles: false, 
//                     stopProcessingIfError: false)
//                 ])
                
//                 script {
                   
                   
//                     def jenkinsUserCause = currentBuild.getBuildCauses('hudson.model.Cause$UserIdCause')
//                     if (!jenkinsUserCause.isEmpty()) {
//                         echo 'Manuel build'
//                     }
                    
//                     def timerCause = currentBuild.getBuildCauses('hudson.triggers.TimerTrigger$TimerTriggerCause')
//                     if (!timerCause.isEmpty()) {
//                         echo 'Nightly build'
//                         // build.log here is created by email extended plugin
//                         emailext attachLog: true, attachmentsPattern: 'build/test/reports/**/*.xml',
//                         body: '$PROJECT_NAME - Jenkins Build # $BUILD_NUMBER - $BUILD_STATUS: Check at ' + '${BUILD_URL}' + 'testReport/ to view the results.',
//                         subject: 'Nightly Jenkins Build # $PROJECT_NAME - $BUILD_NUMBER [$BUILD_STATUS]!', to: 'dajana.velich@gmail.com'
//                     }
                    
//                     def scmCause = currentBuild.getBuildCauses('hudson.triggers.SCMTrigger$SCMTriggerCause')
//                     if (!scmCause.isEmpty()) {
//                         echo 'SCM build'
//                     }
//                 }
                
             

                
//             }
            
//             // changed {
//             //     // there has been change in Jenkins build from the previous one
//             // }

//             success {

//                 // move build and results files in separate folder 
//                 script {
//                     sh 'cd build/test/reports ; mkdir ${BUILD_TAG}-${BUILD_TIMESTAMP} ; mv *.log ${BUILD_TAG}-${BUILD_TIMESTAMP} ; mv *.xml ${BUILD_TAG}-${BUILD_TIMESTAMP}'
//                 }

//                 googleStorageUpload bucket: 'gs://jenkins-onguard-artifacts/',
//                 pathPrefix: 'build/test/reports', // this path part will be stripped from the files uploaded on Google Cloud
//                 pattern: 'build/test/reports/**',
//                 credentialsId: 'Macedonia', // 'Macedonia' is the name of the Google Project that has billing enabled 
//                 sharedPublicly: false,
//                 showInline: true
//             }      

//             // unstable {
//             //     // some tests have failed
//             // }

//             // failure {
//             //     // all tests have failed
//             // }
//         }
// }




